---
layout: vivencias
title: Vicências Tecno-Criativas
---

## O projeto Vivências Tecno-criativas tem por  propósito ser uma imersão criativa para jovens da rede pública de ensino na cidade do Gama-DF, com foco na experimentação com ferramentas tecnológicas de novas mídias.


<h4>CHAMADA PÚBLICA !</h4>


 O projeto Vivências Tecno-criativas torna público o convite para jovens meninas estudantes da cidade da Gama para participarem de uma vivência coletiva com experimentações criativas  no contexto das novas ferramentas tecnológicas.


<details>
<summary> <b style="background: #803a7d; color: #fff; font-size: 15px;">+ Sobre o projeto.</b></summary>
<br>
 <p style=" font-size: 12px;" > O projeto Vivências Tecno-criativas tem por  propósito ser uma imersão criativa para jovens da rede pública de ensino na cidade do Gama-DF, com foco na experimentação com ferramentas tecnológicas de novas mídias . O projeto será realizado em duas etapas, em um  primeiro momento  as jovens passaram por processos formativos, por meio de oficinas, materiais e consultorias,  a fim de desenvolverem habilidades técnicas em tecnologias de mídia (fotografia, vídeo ) e emergentes (impressão 3d, programação e eletrônica), em uma segundo etapa irão participar da organização de uma intervenção criativa e pública, pensando de forma crítica a relação com o espaço público na cidade do Gama .</p>
<p style=" font-size: 12px;" >O objetivo do projeto é promover o deslocamento do estudante para um espaço de experimentação, pesquisa e criação, que possibilite a troca de experiências. Acreditamos que a  criatividade e a inovação passam pelo  convívio e vivências com experiências imersivas, disruptivas e colaborativas entre as pessoas, instituições e o espaço público.</p>
</details>

<details>
<summary> <b style="background: #803a7d; color: #fff; font-size: 15px;">+ Como será executado? </b></summary>
<br>
 <p style=" font-size: 12px;" > As atividades do projeto acontecerão de forma híbrida, com encontros presenciais, totalizando 8 horas semanais e algumas atividades remotas, que completam a carga horária semanal do curso de 10h. A divisão entre as atividades presenciais e remotas podem sofrer alterações, dependendo das recomendações sanitárias. Serão realizados quatro meses de consultorias, aprendizados e conexões.  Dando continuidade ao projeto, os jovens e os monitores participantes, produziram uma intervenção urbana criativa pública na cidade, em colaboração com a comunidade. 
</p>
</details>


<details>
<summary> <b style="background: #803a7d; color: #fff; font-size: 15px;">+ Quem pode se inscrever ? </b></summary>
<br>
 <p style=" font-size: 12px;" >Esta edição será exclusiva para jovens meninas estudantes da rede pública de ensino da cidade do Gama, preferencialmente no nível médio, que se interesse por tecnologias, criação artistas e outras manifestações culturais. Será dado preferência às jovens que se encontram em situação de vulnerabilidade econômica e social.</p>
</details>

<details>
<summary> <b style="background: #803a7d; color: #fff; font-size: 15px;">+ Como posso me inscrever ?</b></summary>
<br>
 <p style=" font-size: 12px;" > As estudantes interessadas deverão preencher o <a href="https://forms.gle/cybKpxbyPvoSzyv7A"  target="_blank">formulário único</a> de inscrição, este formulário será avaliado pela comissão julgadora, observando os critérios de alinhamento com as ideias do projeto e potencial criativo. </p>
  <p style=" font-size: 12px;" > Devido às condições experimentais e escassez de recursos, o projeto pretende atuar neste momento em uma escala micro, atendendo 6 jovens, sendo 2 vagas exclusivas para canditados autodeclaros negros que se encontram em situação de vulnerabilidade social. Todas as alunas selecionados receberão uma ajuda de custo para o transporte e alimentação.
As estudantes habilitadas para participar do projeto serão comunicados logo após a avaliação e haverá a publicação da lista dos aprovados <b>nesta página</b>. As aprovadas terão o prazo de 3 dias úteis para responder à convocação por parte dos organizadores, caso o residente descomprimir alguma norma ou orientação acordada com a organização do projeto, o mesmo poderá ser desligado a qualquer momento.
</p>
</details>
<br>
<b style="color: #803a7d; font-size: 15px;">Datas Importantes</b>

ATIVIDADES | DIAS
--------- | ------
Inscrição (via formulário)         &nbsp; | 06/03 - 31/03
Resultado da Seleção     | 03/04
Início do Projeto      | 14/04


<br><br><br>

